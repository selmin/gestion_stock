package com.projet.backgestion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class BackgestionApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackgestionApplication.class, args);
	}

}
