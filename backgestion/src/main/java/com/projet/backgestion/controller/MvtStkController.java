package com.projet.backgestion.controller;

import com.projet.backgestion.dto.MvtStkDto;
import com.projet.backgestion.services.MvtStkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

import static com.projet.backgestion.utils.Constants.APP_ROOT;

@RestController
@RequestMapping(APP_ROOT)
public class MvtStkController {
    private MvtStkService service;

    @Autowired
    public MvtStkController(MvtStkService service) {
        this.service = service;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/mvtstk/stockreel/{idArticle}")
    public BigDecimal stockReelArticle(@PathVariable("idArticle") Integer idArticle) {
        return service.stockReelArticle(idArticle);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/mvtstk/filter/article/{idArticle}")
    public List<MvtStkDto> mvtStkArticle(@PathVariable("idArticle") Integer idArticle) {
        return service.mvtStkArticle(idArticle);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(path = "/mvtstk/entree")
    public MvtStkDto entreeStock(@RequestBody MvtStkDto dto) {
        return service.entreeStock(dto);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(path = "/mvtstk/sortie")
    public MvtStkDto sortieStock(@RequestBody MvtStkDto dto) {
        return service.sortieStock(dto);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(path = "/mvtstk/correctionpos")
    public MvtStkDto correctionStockPos(@RequestBody MvtStkDto dto) {
        return service.correctionStockPos(dto);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(path = "/mvtstk/correctionneg")
    public MvtStkDto correctionStockNeg(@RequestBody MvtStkDto dto) {
        return service.correctionStockNeg(dto);
    }
}
