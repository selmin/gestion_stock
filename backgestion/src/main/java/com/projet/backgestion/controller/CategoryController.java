package com.projet.backgestion.controller;


import com.projet.backgestion.dto.CategoryDto;
import com.projet.backgestion.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.projet.backgestion.utils.Constants.APP_ROOT;
import static com.projet.backgestion.utils.Constants.UTILISATEUR_ENDPOINT;

@RestController
@RequestMapping(APP_ROOT)
public class CategoryController {

    private CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(path = "/categories/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public CategoryDto save(@RequestBody CategoryDto dto) {
        return categoryService.save(dto);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/categories/{idCategory}", produces = MediaType.APPLICATION_JSON_VALUE)
    public CategoryDto findById(@PathVariable("idCategory") Integer idCategory) {
        return categoryService.findById(idCategory);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/categories/filter/{codeCategory}", produces = MediaType.APPLICATION_JSON_VALUE)
    public CategoryDto findByCode(@PathVariable("codeCategory") String codeCategory) {
        return categoryService.findByCode(codeCategory);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/categories/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CategoryDto> findAll() {
        return categoryService.findAll();
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping(path = "/categories/delete/{idCategory}")
    public void delete(@PathVariable("idCategory")Integer id) {
        categoryService.delete(id);
    }
}
