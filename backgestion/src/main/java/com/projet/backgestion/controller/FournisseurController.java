package com.projet.backgestion.controller;

import com.projet.backgestion.dto.FournisseurDto;
import com.projet.backgestion.services.FournisseurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.projet.backgestion.utils.Constants.APP_ROOT;
import static com.projet.backgestion.utils.Constants.FOURNISSEUR_ENDPOINT;

@RestController
@RequestMapping(FOURNISSEUR_ENDPOINT)
public class FournisseurController {
    private FournisseurService fournisseurService;

    @Autowired
    public FournisseurController(FournisseurService fournisseurService) {
        this.fournisseurService = fournisseurService;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(path = "/create")
    public FournisseurDto save(@RequestBody FournisseurDto dto) {
        return fournisseurService.save(dto);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/{idFournisseur}")
    public FournisseurDto findById(@PathVariable("idFournisseur") Integer id) {
        return fournisseurService.findById(id);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/all")
    public List<FournisseurDto> findAll() {
        return fournisseurService.findAll();
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping(path = "/delete/{idFournisseur}")
    public void delete(@PathVariable("idFournisseur") Integer id) {
        fournisseurService.delete(id);
    }
}
