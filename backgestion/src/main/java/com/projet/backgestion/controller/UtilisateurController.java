package com.projet.backgestion.controller;


import com.projet.backgestion.dto.UtilisateurDto;
import com.projet.backgestion.services.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


import java.util.List;

import static com.projet.backgestion.utils.Constants.UTILISATEUR_ENDPOINT;

@RestController
@RequestMapping(UTILISATEUR_ENDPOINT)
public class UtilisateurController {

    private UtilisateurService utilisateurService;

    @Autowired
    public UtilisateurController(UtilisateurService utilisateurService) {
        this.utilisateurService = utilisateurService;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/all")
    public List<UtilisateurDto> findAll(){
        return utilisateurService.findAll();
    }


    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(path = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public UtilisateurDto save(@RequestBody UtilisateurDto dto){
        return utilisateurService.save(dto);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path ="/{idUtilisateur}")
    public UtilisateurDto findById(@PathVariable("idUtilisateur") Integer id){
        return utilisateurService.findById(id);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path ="/find/{email}")
    public UtilisateurDto findByEmail(@PathVariable("email") String email){
        return utilisateurService.findByEmail(email);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping(path ="/delete/{idUtilisateur}")
    public void delete(@PathVariable("idUtilisateur") Integer id){
        utilisateurService.delete(id);
    }
}
