package com.projet.backgestion.controller;

import com.projet.backgestion.dto.ArticleDto;
import com.projet.backgestion.dto.LigneCommandeClientDto;
import com.projet.backgestion.dto.LigneCommandeFournisseurDto;
import com.projet.backgestion.dto.LigneVenteDto;
import com.projet.backgestion.services.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.projet.backgestion.utils.Constants.APP_ROOT;

@RestController
@RequestMapping(APP_ROOT)
public class ArticleController {
    private ArticleService articleService;

    @Autowired
    public ArticleController(
            ArticleService articleService
    ) {
        this.articleService = articleService;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(path = "/articles/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ArticleDto save(@RequestBody ArticleDto dto) {
        return articleService.save(dto);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/articles/{idArticle}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ArticleDto findById(@PathVariable("idArticle") Integer id) {
        return articleService.findById(id);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/articles/filter/{codeArticle}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ArticleDto findByCodeArticle(@PathVariable("codeArticle") String codeArticle) {
        return articleService.findByCodeArticle(codeArticle);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/articles/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ArticleDto> findAll() {
        return articleService.findAll();
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/articles/historique/vente/{idArticle}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<LigneVenteDto> findHistoriqueVentes(@PathVariable("idArticle") Integer idArticle) {
        return articleService.findHistoriqueVentes(idArticle);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/articles/historique/commandeclient/{idArticle}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<LigneCommandeClientDto> findHistoriaueCommandeClient(@PathVariable("idArticle") Integer idArticle) {
        return articleService.findHistoriaueCommandeClient(idArticle);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/articles/historique/commandefournisseur/{idArticle}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<LigneCommandeFournisseurDto> findHistoriqueCommandeFournisseur(@PathVariable("idArticle") Integer idArticle) {
        return articleService.findHistoriqueCommandeFournisseur(idArticle);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/articles/filter/category/{idCategory}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ArticleDto> findAllArticleByIdCategory(@PathVariable("idCategory") Integer idCategory) {
        return articleService.findAllArticleByIdCategory(idCategory);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping(path = "/articles/delete/{idArticle}")
    public void delete(@PathVariable("idArticle") Integer id) {
        articleService.delete(id);
    }
}
