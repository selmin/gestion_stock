package com.projet.backgestion.controller;

import com.projet.backgestion.dto.CommandeFournisseurDto;
import com.projet.backgestion.dto.LigneCommandeFournisseurDto;
import com.projet.backgestion.model.EtatCommande;
import com.projet.backgestion.services.CommandeFournisseurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

import static com.projet.backgestion.utils.Constants.APP_ROOT;
import static com.projet.backgestion.utils.Constants.COMMANDE_FOURNISSEUR_ENDPOINT;

@RestController
@RequestMapping(COMMANDE_FOURNISSEUR_ENDPOINT)
public class CommandeFournisseurController {
    private CommandeFournisseurService commandeFournisseurService;

    @Autowired
    public CommandeFournisseurController(CommandeFournisseurService commandeFournisseurService) {
        this.commandeFournisseurService = commandeFournisseurService;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(path = "/create")
    public CommandeFournisseurDto save(@RequestBody CommandeFournisseurDto dto) {
        return commandeFournisseurService.save(dto);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PatchMapping(path = "/update/etat/{idCommande}/{etatCommande}")
    public CommandeFournisseurDto updateEtatCommande(@PathVariable("idCommande") Integer idCommande, @PathVariable("etatCommande") EtatCommande etatCommande) {
        return commandeFournisseurService.updateEtatCommande(idCommande, etatCommande);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PatchMapping(path = "/update/quantite/{idCommande}/{idLigneCommande}/{quantite}")
    public CommandeFournisseurDto updateQuantiteCommande(@PathVariable("idCommande") Integer idCommande,
                                                         @PathVariable("idLigneCommande") Integer idLigneCommande, @PathVariable("quantite") BigDecimal quantite) {
        return commandeFournisseurService.updateQuantiteCommande(idCommande, idLigneCommande, quantite);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PatchMapping(path = "/update/fournisseur/{idCommande}/{idFournisseur}")
    public CommandeFournisseurDto updateFournisseur(@PathVariable("idCommande") Integer idCommande, @PathVariable("idFournisseur") Integer idFournisseur) {
        return commandeFournisseurService.updateFournisseur(idCommande, idFournisseur);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PatchMapping(path = "/update/article/{idCommande}/{idLigneCommande}/{idArticle}")
    public CommandeFournisseurDto updateArticle(@PathVariable("idCommande") Integer idCommande,
                                                @PathVariable("idLigneCommande") Integer idLigneCommande, @PathVariable("idArticle") Integer idArticle) {
        return commandeFournisseurService.updateArticle(idCommande, idLigneCommande, idArticle);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping(path = "/delete/article/{idCommande}/{idLigneCommande}")
    public CommandeFournisseurDto deleteArticle(@PathVariable("idCommande") Integer idCommande, @PathVariable("idLigneCommande") Integer idLigneCommande) {
        return commandeFournisseurService.deleteArticle(idCommande, idLigneCommande);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/{idCommandeFournisseur}")
    public CommandeFournisseurDto findById(@PathVariable("idCommandeFournisseur") Integer id) {
        return commandeFournisseurService.findById(id);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/filter/{codeCommandeFournisseur}")
    public CommandeFournisseurDto findByCode(@PathVariable("codeCommandeFournisseur") String code) {
        return commandeFournisseurService.findByCode(code);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/all")
    public List<CommandeFournisseurDto> findAll() {
        return commandeFournisseurService.findAll();
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/lignesCommande/{idCommande}")
    public List<LigneCommandeFournisseurDto> findAllLignesCommandesFournisseurByCommandeFournisseurId(@PathVariable("idCommande") Integer idCommande) {
        return commandeFournisseurService.findAllLignesCommandesFournisseurByCommandeFournisseurId(idCommande);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping(path = "/delete/{idCommandeFournisseur}")
    public void delete(@PathVariable("idCommandeFournisseur") Integer id) {
        commandeFournisseurService.delete(id);
    }
}
