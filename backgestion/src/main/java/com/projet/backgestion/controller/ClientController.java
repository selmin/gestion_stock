package com.projet.backgestion.controller;

import com.projet.backgestion.dto.ClientDto;
import com.projet.backgestion.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.projet.backgestion.utils.Constants.APP_ROOT;

@RestController
@RequestMapping(APP_ROOT)
public class ClientController {

    private ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(path = "/clients/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ClientDto save(@RequestBody ClientDto dto) {
        return clientService.save(dto);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/clients/{idClient}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ClientDto findById(@PathVariable("idClient") Integer id) {
        return clientService.findById(id);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/clients/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ClientDto> findAll() {
        return clientService.findAll();
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping(path = "/clients/delete/{idClient}")
    public void delete(@PathVariable("idClient") Integer id) {
        clientService.delete(id);
    }
}
