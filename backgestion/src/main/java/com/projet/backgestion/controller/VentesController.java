package com.projet.backgestion.controller;

import com.projet.backgestion.dto.VentesDto;
import com.projet.backgestion.services.VentesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.projet.backgestion.utils.Constants.APP_ROOT;
import static com.projet.backgestion.utils.Constants.VENTES_ENDPOINT;

@RestController
@RequestMapping(VENTES_ENDPOINT)
public class VentesController {
    private VentesService ventesService;

    @Autowired
    public VentesController(VentesService ventesService) {
        this.ventesService = ventesService;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(path = "/create")
    public VentesDto save(@RequestBody VentesDto dto) {
        return ventesService.save(dto);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/{idVente}")
    public VentesDto findById(@PathVariable("idVente") Integer id) {
        return ventesService.findById(id);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/{codeVente}")
    public VentesDto findByCode(@PathVariable("codeVente") String code) {
        return ventesService.findByCode(code);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/all")
    public List<VentesDto> findAll() {
        return ventesService.findAll();
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping(path = "/delete/{idVente}")
    public void delete(@PathVariable("idVente") Integer id) {
        ventesService.delete(id);
    }
}
