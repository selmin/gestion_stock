package com.projet.backgestion.controller;

import com.projet.backgestion.dto.CommandeClientDto;
import com.projet.backgestion.dto.LigneCommandeClientDto;
import com.projet.backgestion.model.EtatCommande;
import com.projet.backgestion.services.CommandeClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

import static com.projet.backgestion.utils.Constants.APP_ROOT;

@RestController
@RequestMapping(APP_ROOT)
public class CommandeClientController {
    private CommandeClientService commandeClientService;

    @Autowired
    public CommandeClientController(CommandeClientService commandeClientService) {
        this.commandeClientService = commandeClientService;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(path = "/commandesclients/create")
    public ResponseEntity<CommandeClientDto> save(@RequestBody CommandeClientDto dto) {
        return ResponseEntity.ok(commandeClientService.save(dto));
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PatchMapping(path = "/commandesclients/update/etat/{idCommande}/{etatCommande}")
    public ResponseEntity<CommandeClientDto> updateEtatCommande(@PathVariable("idCommande") Integer idCommande, @PathVariable("etatCommande") EtatCommande etatCommande) {
        return ResponseEntity.ok(commandeClientService.updateEtatCommande(idCommande, etatCommande));
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PatchMapping(path = "/commandesclients/update/quantite/{idCommande}/{idLigneCommande}/{quantite}")
    public ResponseEntity<CommandeClientDto> updateQuantiteCommande(@PathVariable("idCommande") Integer idCommande,
                                                                    @PathVariable("idLigneCommande") Integer idLigneCommande, @PathVariable("quantite") BigDecimal quantite) {
        return ResponseEntity.ok(commandeClientService.updateQuantiteCommande(idCommande, idLigneCommande, quantite));
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PatchMapping(path = "/commandesclients/update/client/{idCommande}/{idClient}")
    public ResponseEntity<CommandeClientDto> updateClient(@PathVariable("idCommande") Integer idCommande, @PathVariable("idClient") Integer idClient) {
        return ResponseEntity.ok(commandeClientService.updateClient(idCommande, idClient));
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PatchMapping(path = "/commandesclients/update/article/{idCommande}/{idLigneCommande}/{idArticle}")
    public ResponseEntity<CommandeClientDto> updateArticle(@PathVariable("idCommande") Integer idCommande,
                                                           @PathVariable("idLigneCommande") Integer idLigneCommande, @PathVariable("idArticle") Integer idArticle) {
        return ResponseEntity.ok(commandeClientService.updateArticle(idCommande, idLigneCommande, idArticle));
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping(path = "/commandesclients/delete/article/{idCommande}/{idLigneCommande}")
    public ResponseEntity<CommandeClientDto> deleteArticle(@PathVariable("idCommande") Integer idCommande, @PathVariable("idLigneCommande") Integer idLigneCommande) {
        return ResponseEntity.ok(commandeClientService.deleteArticle(idCommande, idLigneCommande));
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/commandesclients/{idCommandeClient}")
    public ResponseEntity<CommandeClientDto> findById(@PathVariable("idCommandeClient") Integer id) {
        return ResponseEntity.ok(commandeClientService.findById(id));
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/commandesclients/filter/{codeCommandeClient}")
    public ResponseEntity<CommandeClientDto> findByCode(@PathVariable("codeCommandeClient") String code) {
        return ResponseEntity.ok(commandeClientService.findByCode(code));
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/commandesclients/all")
    public ResponseEntity<List<CommandeClientDto>> findAll() {
        return ResponseEntity.ok(commandeClientService.findAll());
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/commandesclients/lignesCommande/{idCommande}")
    public ResponseEntity<List<LigneCommandeClientDto>> findAllLignesCommandesClientByCommandeClientId(@PathVariable("idCommande") Integer idCommande) {
        return ResponseEntity.ok(commandeClientService.findAllLignesCommandesClientByCommandeClientId(idCommande));
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping(path = "/commandesclients/delete/{idCommandeClient}")
    public ResponseEntity<Void> delete(@PathVariable("idCommandeClient") Integer id) {
        commandeClientService.delete(id);
        return ResponseEntity.ok().build();
    }
}
