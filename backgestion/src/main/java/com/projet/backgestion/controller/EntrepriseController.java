package com.projet.backgestion.controller;

import com.projet.backgestion.dto.EntrepriseDto;
import com.projet.backgestion.services.EntrepriseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.projet.backgestion.utils.Constants.APP_ROOT;

@RestController
@RequestMapping(APP_ROOT)
public class EntrepriseController {

    private EntrepriseService entrepriseService;

    @Autowired
    public EntrepriseController(EntrepriseService entrepriseService) {
        this.entrepriseService = entrepriseService;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(path = "/entreprises/create",consumes = {"application/xml", "application/json"})
    public EntrepriseDto save(@RequestBody EntrepriseDto dto) {
        return entrepriseService.save(dto);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/entreprises/{idEntreprise}")
    public EntrepriseDto findById(@PathVariable("idEntreprise") Integer id) {
        return entrepriseService.findById(id);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/entreprises/all")
    public List<EntrepriseDto> findAll() {
        return entrepriseService.findAll();
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping(path = "/entreprises/delete/{idEntreprise}")
    public void delete(@PathVariable("idEntreprise") Integer id) {
        entrepriseService.delete(id);
    }
}
