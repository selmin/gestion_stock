package com.projet.backgestion.model;

public enum SourceMvtStk {
    COMMANDE_CLIENT,
    COMMANDE_FOURNISSEUR,
    VENTE
}
