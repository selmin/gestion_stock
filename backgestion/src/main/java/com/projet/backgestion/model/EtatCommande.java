package com.projet.backgestion.model;

public enum EtatCommande {

    EN_PREPARATION,
    VALIDEE,
    LIVREE
}
