package com.projet.backgestion.repository;

import com.projet.backgestion.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Integer> {

}
