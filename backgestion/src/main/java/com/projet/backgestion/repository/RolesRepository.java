package com.projet.backgestion.repository;

import com.projet.backgestion.model.Roles;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RolesRepository extends JpaRepository<Roles, Integer> {

}
