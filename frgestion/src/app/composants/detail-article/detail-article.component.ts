import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ArticleService } from 'src/app/services/article/article.service';
import { ArticleDto } from 'src/gs-api/src/models';

@Component({
  selector: 'app-detail-article',
  templateUrl: './detail-article.component.html',
  styleUrls: ['./detail-article.component.scss']
})
export class DetailArticleComponent implements OnInit {
  @Input()
  articleDto : ArticleDto ={};

  constructor(private router : Router,
    private articleService : ArticleService) { }

  ngOnInit(): void {
  }
  supprimer(id : number){
    return this.articleService.delete(id).subscribe(res=>{
      this.router.navigate(['articles']).then(() => {
        window.location.reload();
      });
    });
  }

}
