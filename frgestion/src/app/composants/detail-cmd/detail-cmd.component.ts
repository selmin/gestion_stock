import { Component, Input, OnInit } from '@angular/core';
import { LigneCmdService } from 'src/app/services/ligne-cmd/ligne-cmd.service';

@Component({
  selector: 'app-detail-cmd',
  templateUrl: './detail-cmd.component.html',
  styleUrls: ['./detail-cmd.component.scss']
})
export class DetailCmdComponent implements OnInit {
  @Input()
  ligneCommande : any ={};
  @Input()
  idCommande =0;

  constructor(private ligneCmdService : LigneCmdService) { }

  ngOnInit(): void {
  }

}
