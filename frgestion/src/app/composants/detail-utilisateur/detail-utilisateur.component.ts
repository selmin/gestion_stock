import { Component, Input, OnInit } from '@angular/core';
import { UtilisateurDto } from 'src/gs-api/src/models';

@Component({
  selector: 'app-detail-utilisateur',
  templateUrl: './detail-utilisateur.component.html',
  styleUrls: ['./detail-utilisateur.component.scss']
})
export class DetailUtilisateurComponent implements OnInit {

  @Input()
  utilisateurDto : UtilisateurDto ={};

  constructor() { }

  ngOnInit(): void {
  }

}
