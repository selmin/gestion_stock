import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ClientService } from 'src/app/services/client/client.service';
import { FournisseurService } from 'src/app/services/fournisseur/fournisseur.service';

@Component({
  selector: 'app-detail-clt-frs',
  templateUrl: './detail-clt-frs.component.html',
  styleUrls: ['./detail-clt-frs.component.scss']
})
export class DetailCltFrsComponent implements OnInit {

  @Input()
  clientFournisseur : any = {};

  @Input()
  origin ='';

  constructor(
    private router: Router,
    private clientService : ClientService,
    private fournisseurService : FournisseurService
  ) { }

  ngOnInit(): void {
  }

  supprimer(id : number):void{
    if (this.origin === 'client') {
      this.clientService.delete(id)
      .subscribe(data => {
        this.router.navigate(['clients']).then(() => {
          window.location.reload();
        });
      });
    } else if (this.origin === 'fournisseur') {
      this.fournisseurService.delete(id)
      .subscribe(data => {
        this.router.navigate(['fournisseurs']).then(() => {
          window.location.reload();
        });
      });
    }
  }

}


