import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ArticleService } from 'src/app/services/article/article.service';
import { ClientService } from 'src/app/services/client/client.service';
import { CmdCltService } from 'src/app/services/cmd-clt/cmd-clt.service';
import { CmdFrsService } from 'src/app/services/cmd-frs/cmd-frs.service';
import { FournisseurService } from 'src/app/services/fournisseur/fournisseur.service';
import { ArticleDto, CommandeClientDto, CommandeFournisseurDto, LigneCommandeClient, LigneCommandeClientDto } from 'src/gs-api/src/models';

@Component({
  selector: 'app-nouvelle-cmd-clt-frs',
  templateUrl: './nouvelle-cmd-clt-frs.component.html',
  styleUrls: ['./nouvelle-cmd-clt-frs.component.scss']
})
export class NouvelleCmdCltFrsComponent implements OnInit {
  
  origin = '';
  errorMsg: Array<string> = [];
  selectedClientFournisseur : any ={};
  listClientsFournisseurs : Array<any> =[];
  listArticle : Array<any> =[];
  searchedArticle : ArticleDto ={};
  codeArticle ='';
  quantite ='';
  codeCommande ='';
  lignesCommande : Array<any> = [];
  totalCommande = 0;
  articleNotYetSelected = false;
  

  constructor(
    private activatedRoute : ActivatedRoute,
    private router : Router,
    private clientService : ClientService,
    private fournisseurService : FournisseurService,
    private articleService : ArticleService,
    private cmdFrsService : CmdFrsService,
    private cmdCltService : CmdCltService
  ) { }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.origin = data['origin'];
    });
    this.findAll();
    this.findAllArticles();
  }

  findAll():void{
    if (this.origin === 'client') {
      this.clientService.get()
      .subscribe(data => {
        this.listClientsFournisseurs = data;
      });
    } else if (this.origin === 'fournisseur') {
      this.fournisseurService.get()
      .subscribe(data => {
        this.listClientsFournisseurs = data;
      });
    }
  }

  findAllArticles():void{
    this.articleService.get()
    .subscribe(data=>{
      this.listArticle =data;
    });
  }

  findArticleByCode(codeArticle : string):void{
    this.articleService.findByCode(codeArticle)
    .subscribe(article=>{
      this.searchedArticle = article;
    });
  }

  filtrerArticle() : void{
    if (this.codeArticle.length === 0) {
      this.findAllArticles();
    }
    this.listArticle = this.listArticle
    .filter(art => art.codeArticle?.includes(this.codeArticle) || art.designation?.includes(this.codeArticle));
  }

  ajouterLigneCommande(): void {
    this.checkLigneCommande();
    this.calculerTotalCommande();

    this.searchedArticle = {};
    this.quantite = '';
    this.codeArticle = '';
    this.articleNotYetSelected = false;
    this.findAllArticles();
  }

  calculerTotalCommande(): void {
    this.totalCommande = 0;
    this.lignesCommande.forEach(ligne => {
      if (ligne.prixUnitaire && ligne.quantite) {
        this.totalCommande += +ligne.prixUnitaire * +ligne.quantite;
      }
    });
  }
  private checkLigneCommande(): void {
    const ligneCmdAlreadyExists = this.lignesCommande.find(lig => lig.article?.codeArticle === this.searchedArticle.codeArticle);
    if (ligneCmdAlreadyExists) {
      this.lignesCommande.forEach(lig => {
        if (lig && lig.article?.codeArticle === this.searchedArticle.codeArticle) {
          // @ts-ignore
          lig.quantite = lig.quantite + +this.quantite;
        }
      });
    } else {
      const ligneCmd: LigneCommandeClientDto = {
        article: this.searchedArticle,
        prixUnitaire: this.searchedArticle.prixUnitaireTtc,
        quantite: +this.quantite
      };
      this.lignesCommande.push(ligneCmd);
    }
  }


  selectArticleClick(article : ArticleDto):void{
    this.searchedArticle = article;
    this.codeArticle = article.codeArticle ? article.codeArticle : '';
    this.articleNotYetSelected = true;
  }

  enregistrerCommande(): void {
    const commande = this.preparerCommande();
    if (this.origin === 'client') {
      this.cmdCltService.save(commande as CommandeClientDto)
      .subscribe(cmd => {
        console.log(cmd);
        this.router.navigate(['commandesclient']);
      });
    } else if (this.origin === 'fournisseur') {
      this.cmdFrsService.save(commande as CommandeFournisseurDto)
      .subscribe(cmd => {
        this.router.navigate(['commandesfournisseur']);
      });
    }
  }


  private preparerCommande(): any {
    if (this.origin === 'client') {
      return  {
        client: this.selectedClientFournisseur,
        code: this.codeCommande,
        dateCommande: new Date().getTime(),
        etatCommande: 'EN_PREPARATION',
        idEntreprise : 1,
        ligneCommandeClients: this.lignesCommande
      };
    } else if (this.origin === 'fournisseur') {
      return  {
        fournisseur: this.selectedClientFournisseur,
        code: this.codeCommande,
        dateCommande: new Date().getTime(),
        etatCommande: 'EN_PREPARATION',
        idEntreprise : 1,
        ligneCommandeFournisseurs: this.lignesCommande
      };
    }
  }


}
