import { Component, OnInit } from '@angular/core';
import { DataServiceService } from 'src/app/services/data-service/data-service.service';
import { MvtStkService } from 'src/app/services/mvt-stk/mvt-stk.service';

@Component({
  selector: 'app-detail-mvt-stk',
  templateUrl: './detail-mvt-stk.component.html',
  styleUrls: ['./detail-mvt-stk.component.scss']
})
export class DetailMvtStkComponent implements OnInit {

  articleId =0;
  listMvts : any =[];

  constructor(private dataService : DataServiceService,
    private mvtStkService : MvtStkService) { }

  ngOnInit(): void {
    this.getArticleId();
    this.getListMvts();
  }

  getArticleId():void{
    this.articleId = this.dataService.getArticleCode();
    this.dataService.setArticleCode(0);
  }

  getListMvts(){
    if(this.articleId !==0){
      this.mvtStkService.getById(this.articleId)
      .subscribe(data=>{
        this.listMvts = data;
      });
    }
  }



}
