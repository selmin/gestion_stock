import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ClientService } from 'src/app/services/client/client.service';
import { FournisseurService } from 'src/app/services/fournisseur/fournisseur.service';
import { AdresseDto, ClientDto, FournisseurDto } from 'src/gs-api/src/models';

@Component({
  selector: 'app-nouveau-clt-frs',
  templateUrl: './nouveau-clt-frs.component.html',
  styleUrls: ['./nouveau-clt-frs.component.scss']
})
export class NouveauCltFrsComponent implements OnInit {
  origin = '';
  errorMsg: Array<string> = [];
  clientFournisseur : any ={};
  adresseDto : AdresseDto = {};
  clientDto : ClientDto = {};
  fournisseurDto : FournisseurDto = {};

  constructor(
    private router: Router,
    private activatedRoute : ActivatedRoute,
    private clientService : ClientService,
    private fournisseurService : FournisseurService
  ) { }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.origin = data['origin'];
    });
  }

  cancelClick(): void {
    if (this.origin === 'client') {
      this.router.navigate(['clients']);
    } else if (this.origin === 'fournisseur') {
      this.router.navigate(['fournisseurs']);
    }
  }

  enregistrerCltFrs():void{
    if (this.origin === 'client') {
      this.clientDto = this.clientFournisseur;
      this.clientDto.idEntreprise =1;
      this.clientDto.adresse = this.adresseDto;
      this.clientService.save(this.clientDto)
      .subscribe(data => {
        this.router.navigate(['clients']);
      });
    } else if (this.origin === 'fournisseur') {
      this.fournisseurDto = this.clientFournisseur;
      this.fournisseurDto.idEntreprise =1;
      this.fournisseurDto.adresse = this.adresseDto;
      this.fournisseurService.save(this.fournisseurDto)
      .subscribe(data => {
        this.router.navigate(['fournisseurs']);
      });
    }
  }

}
