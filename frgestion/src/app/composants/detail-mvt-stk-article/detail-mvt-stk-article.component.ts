import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { DataServiceService } from 'src/app/services/data-service/data-service.service';

@Component({
  selector: 'app-detail-mvt-stk-article',
  templateUrl: './detail-mvt-stk-article.component.html',
  styleUrls: ['./detail-mvt-stk-article.component.scss']
})
export class DetailMvtStkArticleComponent implements OnInit {

  @Input()
  article : any = {};
  @Output()
  eventToFirst = new EventEmitter();

  constructor(private dataService : DataServiceService,
    private router : Router) { }

  ngOnInit(): void {
  }

  sendCodeArticle(id : number){
    console.log(id);
    this.dataService.setArticleCode(id);
    this.eventToFirst.emit('lancer event');
  }

}
