import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CommandeClientDto } from 'src/gs-api/src/models';

@Injectable({
  providedIn: 'root'
})
export class CmdCltService {

  private saveURL ="http://localhost:8080/gestiondestock/v1/commandesclients/create";
  private getURL ="http://localhost:8080/gestiondestock/v1/commandesclients/all";
  private deleteURL ="http://localhost:8080/gestiondestock/v1/commandesclients/delete";

  constructor(private httpClient : HttpClient) { }

  save(commandeClientDto : CommandeClientDto): Observable<Object>{
    return this.httpClient.post(`${this.saveURL}`,commandeClientDto);
  }

  get(): Observable<any>{
    return this.httpClient.get(`${this.getURL}`);
  }

  delete(id:number):Observable<Object>{
    
    return this.httpClient.delete(`${this.deleteURL}/${id}`);
  }
}
