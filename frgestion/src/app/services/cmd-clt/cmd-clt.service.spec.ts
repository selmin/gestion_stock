import { TestBed } from '@angular/core/testing';

import { CmdCltService } from './cmd-clt.service';

describe('CmdCltService', () => {
  let service: CmdCltService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CmdCltService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
