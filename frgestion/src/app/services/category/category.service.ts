import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CategoryDto } from 'src/gs-api/src/models';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private saveURL ="http://localhost:8080/gestiondestock/v1/categories/create";
  private getURL ="http://localhost:8080/gestiondestock/v1/categories/all";
  private deleteURL ="http://localhost:8080/gestiondestock/v1/categories/delete";

  constructor(private httpClient : HttpClient) { }

  save(categoryDto : CategoryDto): Observable<Object>{
    return this.httpClient.post(`${this.saveURL}`,categoryDto);
  }

  get(): Observable<any>{
    return this.httpClient.get(`${this.getURL}`);
  }

  delete(id:number):Observable<Object>{
    
    return this.httpClient.delete(`${this.deleteURL}/${id}`);
  }
}
