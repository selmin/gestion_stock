import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private getURL ="http://localhost:8080/gestiondestock/v1/utilisateurs/all";
  constructor(
    private router: Router,
    private httpClient : HttpClient
  ) { }

  get(): Observable<any>{
    return this.httpClient.get(`${this.getURL}`);
  }
}
