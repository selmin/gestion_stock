import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ArticleDto } from 'src/gs-api/src/models/article-dto';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  private saveURL ="http://localhost:8080/gestiondestock/v1/articles/create";
  private getURL ="http://localhost:8080/gestiondestock/v1/articles/all";
  private deleteURL ="http://localhost:8080/gestiondestock/v1/articles/delete";
  private filterURL ="http://localhost:8080/gestiondestock/v1/articles/filter";

  constructor(private httpClient : HttpClient) { }

  save(articleDto : ArticleDto): Observable<Object>{
    return this.httpClient.post(`${this.saveURL}`,articleDto);
  }

  get(): Observable<any>{
    return this.httpClient.get(`${this.getURL}`);
  }

  delete(id:number):Observable<Object>{
    
    return this.httpClient.delete(`${this.deleteURL}/${id}`);
  }

  findByCode(code : string): Observable<any>{
    return this.httpClient.get(`${this.filterURL}/${code}`);
  }
}
