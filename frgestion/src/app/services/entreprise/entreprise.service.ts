import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EntrepriseDto } from 'src/gs-api/src/models';
import { ApiService } from 'src/gs-api/src/services';

@Injectable({
  providedIn: 'root'
})
export class EntrepriseService {

  private saveURL ="http://localhost:8080/gestiondestock/v1/entreprises/create";

  constructor(private httpClient : HttpClient) { }

  // sinscrire(entreprise : EntrepriseDto): Observable<EntrepriseDto>{
  //   return this.apiService.save_5(entreprise);
  // }

  save(entrepriseDto : EntrepriseDto): Observable<Object>{
    return this.httpClient.post(`${this.saveURL}`,entrepriseDto);
  }
}
