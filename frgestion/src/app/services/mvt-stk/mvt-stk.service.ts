import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MvtStkService {

  private getURL ="http://localhost:8080/gestiondestock/v1/mvtstk/filter/article";

  constructor(private httpClient : HttpClient) { }

  getById(id:number): Observable<any>{
    return this.httpClient.get(`${this.getURL}/${id}`);
  }
}
