import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FournisseurDto } from 'src/gs-api/src/models';

@Injectable({
  providedIn: 'root'
})
export class FournisseurService {

  private saveURL ="http://localhost:8080/gestiondestock/v1/fournisseurs/create";
  private getURL ="http://localhost:8080/gestiondestock/v1/fournisseurs/all";
  private deleteURL ="http://localhost:8080/gestiondestock/v1/fournisseurs/delete";

  constructor(private httpClient : HttpClient) { }

  save(fournisseurDto : FournisseurDto): Observable<Object>{
    return this.httpClient.post(`${this.saveURL}`,fournisseurDto);
  }

  get(): Observable<any>{
    return this.httpClient.get(`${this.getURL}`);
  }

  delete(id:number):Observable<Object>{
    
    return this.httpClient.delete(`${this.deleteURL}/${id}`);
  }
}
