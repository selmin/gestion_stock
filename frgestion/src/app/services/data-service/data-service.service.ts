import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataServiceService {

  articleCode = 0;

  constructor() { }

  setArticleCode(id : number){
    this.articleCode = id;
  }
  getArticleCode():number{
    return this.articleCode;
  }
}
