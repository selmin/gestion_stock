import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ClientDto } from 'src/gs-api/src/models';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  private saveURL ="http://localhost:8080/gestiondestock/v1/clients/create";
  private getURL ="http://localhost:8080/gestiondestock/v1/clients/all";
  private deleteURL ="http://localhost:8080/gestiondestock/v1/clients/delete";

  constructor(private httpClient : HttpClient) { }

  save(clientDto : ClientDto): Observable<Object>{
    return this.httpClient.post(`${this.saveURL}`,clientDto);
  }

  get(): Observable<any>{
    return this.httpClient.get(`${this.getURL}`);
  }

  delete(id:number):Observable<Object>{
    
    return this.httpClient.delete(`${this.deleteURL}/${id}`);
  }
}
