import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CommandeFournisseurDto } from 'src/gs-api/src/models';

@Injectable({
  providedIn: 'root'
})
export class CmdFrsService {

  private saveURL ="http://localhost:8080/gestiondestock/v1/commandesfournisseurs/create";
  private getURL ="http://localhost:8080/gestiondestock/v1/commandesfournisseurs/all";
  private deleteURL ="http://localhost:8080/gestiondestock/v1/commandesfournisseurs/delete";

  constructor(private httpClient : HttpClient) { }

  save(commandeFournisseurDto : CommandeFournisseurDto): Observable<Object>{
    return this.httpClient.post(`${this.saveURL}`,commandeFournisseurDto);
  }

  get(): Observable<any>{
    return this.httpClient.get(`${this.getURL}`);
  }

  delete(id:number):Observable<Object>{
    
    return this.httpClient.delete(`${this.deleteURL}/${id}`);
  }
}
