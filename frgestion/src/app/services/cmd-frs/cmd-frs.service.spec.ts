import { TestBed } from '@angular/core/testing';

import { CmdFrsService } from './cmd-frs.service';

describe('CmdFrsService', () => {
  let service: CmdFrsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CmdFrsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
