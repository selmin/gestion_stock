import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { UtilisateurDto } from 'src/gs-api/src/models';

@Component({
  selector: 'app-page-utilisateur',
  templateUrl: './page-utilisateur.component.html',
  styleUrls: ['./page-utilisateur.component.scss']
})
export class PageUtilisateurComponent implements OnInit {

  utilistaeurDto : UtilisateurDto = {};
  listUtilisateurs : Array<UtilisateurDto> =[];

  constructor(
    private router : Router,
    private userService : UserService
  ) { }

  ngOnInit(): void {
    this.userService.get().subscribe(res=>{
      this.listUtilisateurs = res;
    });
  }

  nouvelUtilosateur() : void {
    this.router.navigate(['nouvelutilisateur']);
  }
}
