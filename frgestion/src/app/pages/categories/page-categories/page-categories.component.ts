import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CategoryService } from 'src/app/services/category/category.service';
import { CategoryDto } from 'src/gs-api/src/models';

@Component({
  selector: 'app-page-categories',
  templateUrl: './page-categories.component.html',
  styleUrls: ['./page-categories.component.scss']
})
export class PageCategoriesComponent implements OnInit {

  listCategories : Array<CategoryDto> =[];

  constructor(
    private router : Router,
    private categoryService : CategoryService
  ) { }

  ngOnInit(): void {
    this.categoryService.get().subscribe(res=>{
      this.listCategories =res;
    });
  }
  nouvelleCategory() : void{
    this.router.navigate(['nouvellecategorie']);
  }

  supprimer(catId : number){
    
      this.categoryService.delete(catId).subscribe(data=>{
        this.router.navigate(['categories']).then(() => {
          window.location.reload();
        });
      })
      
    
  }

}
