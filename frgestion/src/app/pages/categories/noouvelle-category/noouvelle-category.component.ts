import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CategoryService } from 'src/app/services/category/category.service';
import { CategoryDto } from 'src/gs-api/src/models';

@Component({
  selector: 'app-noouvelle-category',
  templateUrl: './noouvelle-category.component.html',
  styleUrls: ['./noouvelle-category.component.scss']
})
export class NoouvelleCategoryComponent implements OnInit {

  categoryDto : CategoryDto = {};

  constructor(
    private router: Router,
    private categoryService : CategoryService
  ) { }

  ngOnInit(): void {
  }

  createCategory():void{
    this.categoryDto.idEntreprise = 1;
    this.categoryService.save(this.categoryDto)
    .subscribe(data => {
      this.router.navigate(['categories']);
    }
    );
  }

  cancel(): void {
    this.router.navigate(['categories']);
  }

}
