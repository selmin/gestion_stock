import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FournisseurService } from 'src/app/services/fournisseur/fournisseur.service';
import { FournisseurDto } from 'src/gs-api/src/models';

@Component({
  selector: 'app-page-fournisseur',
  templateUrl: './page-fournisseur.component.html',
  styleUrls: ['./page-fournisseur.component.scss']
})
export class PageFournisseurComponent implements OnInit {
  listFournisseur : Array<FournisseurDto> = [];

  constructor(
    private router : Router,
    private fournisseurService : FournisseurService
  ) { }

  ngOnInit(): void {
    this.fournisseurService.get().subscribe(res=>{
      this.listFournisseur =res;
    });
  }

  nouveauFournisseur (): void{
    this.router.navigate(['nouveaufournisseur']);
  }

}
