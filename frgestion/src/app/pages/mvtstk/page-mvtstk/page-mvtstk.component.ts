import { Component, OnInit } from '@angular/core';
import { ArticleService } from 'src/app/services/article/article.service';

@Component({
  selector: 'app-page-mvtstk',
  templateUrl: './page-mvtstk.component.html',
  styleUrls: ['./page-mvtstk.component.scss']
})
export class PageMvtstkComponent implements OnInit {

  listArticles : any = [];
  lance='';

  constructor(
    private articleService : ArticleService) { }

  ngOnInit(): void {
    this.articleService.get()
    .subscribe(data=>{
      this.listArticles = data;
    });
  }
  traitement(msg:string){
    this.lance=msg;
  }

}
