import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ClientService } from 'src/app/services/client/client.service';
import { ClientDto } from 'src/gs-api/src/models';

@Component({
  selector: 'app-page-client',
  templateUrl: './page-client.component.html',
  styleUrls: ['./page-client.component.scss']
})
export class PageClientComponent implements OnInit {

  listClient : Array<ClientDto> = [];

  constructor(
    private router : Router,
    private clientService : ClientService
  ) { }

  ngOnInit(): void {
    this.clientService.get().subscribe(res=>{
      this.listClient =res;
    });
  }

  nouveauClient() : void {
    this.router.navigate(['nouveauclient']);
  }

}
