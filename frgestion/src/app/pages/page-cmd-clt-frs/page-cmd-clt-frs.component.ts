import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CmdCltService } from 'src/app/services/cmd-clt/cmd-clt.service';
import { CmdFrsService } from 'src/app/services/cmd-frs/cmd-frs.service';

@Component({
  selector: 'app-page-cmd-clt-frs',
  templateUrl: './page-cmd-clt-frs.component.html',
  styleUrls: ['./page-cmd-clt-frs.component.scss']
})
export class PageCmdCltFrsComponent implements OnInit {

  origin = '';
  listeCommandes : Array<any> =[];

  constructor(
    private router : Router,
    private activatedRoute: ActivatedRoute,
    private cmdCltService : CmdCltService,
    private cmdFrsService : CmdFrsService
  ) { }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.origin = data['origin'];
    });
    this.findAllCommandes();
  }

  findAllCommandes():void{
    if (this.origin === 'client') {
      this.cmdCltService.get()
      .subscribe(data => {
        this.listeCommandes = data;
      });
    } else if (this.origin === 'fournisseur') {
      this.cmdFrsService.get()
      .subscribe(data => {
        this.listeCommandes = data;
      });
    }
  }

  nouvelleCommande(): void {
    if (this.origin === 'client') {
      this.router.navigate(['nouvellecommandeclt']);
    } else if (this.origin === 'fournisseur') {
      this.router.navigate(['nouvellecommandefrs']);
    }
  }

}
