import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EntrepriseService } from 'src/app/services/entreprise/entreprise.service';
import { AdresseDto, EntrepriseDto } from 'src/gs-api/src/models';
import { ApiService } from 'src/gs-api/src/services';

@Component({
  selector: 'app-page-inscription',
  templateUrl: './page-inscription.component.html',
  styleUrls: ['./page-inscription.component.scss']
})
export class PageInscriptionComponent implements OnInit {

  entrepriseDto : EntrepriseDto ={};
  adresse : AdresseDto = {};
  errorsMsg : Array<string> =[];

  constructor(private entrepriseService : EntrepriseService
    ,private router : Router) { }

  ngOnInit(): void {}

  inscrire() : void{
    this.entrepriseDto.adresse = this.adresse;
    console.log(this.entrepriseDto);
    this.entrepriseService.save(this.entrepriseDto)
    .subscribe(data => {
      this.router.navigate(['login']);
    }
    );
  }

}


