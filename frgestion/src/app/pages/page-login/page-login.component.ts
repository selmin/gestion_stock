import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-page-login',
  templateUrl: './page-login.component.html',
  styleUrls: ['./page-login.component.scss']
})
export class PageLoginComponent implements OnInit {

  loginn ='';
  password ='';
  errorMessage ='';

  constructor(private router : Router) { }

  ngOnInit(): void {
  }

  login():void{
    if(this.loginn === 'sami@sst.tn' && this.password ==='azertyqw'){
      this.router.navigate(['']);
    }else{
      this.errorMessage ='Veuillez verifier votre Email ou Mot de passe';
    }
  }

}
