import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ArticleService } from 'src/app/services/article/article.service';
import { ArticleDto } from 'src/gs-api/src/models';

@Component({
  selector: 'app-page-article',
  templateUrl: './page-article.component.html',
  styleUrls: ['./page-article.component.scss']
})
export class PageArticleComponent implements OnInit {

  listArticle : Array<ArticleDto> = [];

  constructor(
    private router: Router,
    private articleservice : ArticleService
  ) { }

  ngOnInit(): void {
    this.articleservice.get().subscribe(res=>{
      this.listArticle =res;
      console.log(res);
    });
  }
  nouvelArticle(): void {
    this.router.navigate(['nouvelarticle']);
  }

}
