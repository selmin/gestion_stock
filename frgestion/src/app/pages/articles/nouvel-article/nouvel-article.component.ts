import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ArticleService } from 'src/app/services/article/article.service';
import { CategoryService } from 'src/app/services/category/category.service';
import { ArticleDto, CategoryDto } from 'src/gs-api/src/models';

@Component({
  selector: 'app-nouvel-article',
  templateUrl: './nouvel-article.component.html',
  styleUrls: ['./nouvel-article.component.scss']
})
export class NouvelArticleComponent implements OnInit {

  articleDto : ArticleDto = {};
  categoryDto : CategoryDto ={};
  listCategories : Array<CategoryDto> = [];

  errorMsg: Array<string> = [];
  file: File | null = null;
  imgUrl: string | ArrayBuffer = 'assets/product.png';

  constructor(
    private router: Router,
    private articleService : ArticleService,
    private categoryService : CategoryService
  ) { }

  ngOnInit(): void {
    this.categoryService.get().subscribe(res=>{
      this.listCategories =res;
    });
  }

  cancel(): void {
    this.router.navigate(['articles']);
  }

  enregistrerArticle():void{
    this.articleDto.idEntreprise =1;
    this.articleDto.category = this.categoryDto;
    this.articleService.save(this.articleDto)
    .subscribe(data => {
      this.router.navigate(['articles']);
    }
    );
  }

}
